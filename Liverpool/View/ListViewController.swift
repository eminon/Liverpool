//
//  ViewController.swift
//  Liverpool
//
//  Created by Eduardo Miñon on 16/02/24.
//

import UIKit
import SDWebImage

class ListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var itemViewModel = ItemViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Liverpool"
        initViewModel()
    }
    
    func initViewModel(){
        itemViewModel.reloadTableView = {
            DispatchQueue.main.async { self.tableView.reloadData() }
        }
        itemViewModel.showError = {
            DispatchQueue.main.async { self.showAlert("Error") }
        }
        itemViewModel.showLoading = {
            DispatchQueue.main.async { self.activityIndicator.startAnimating() }
        }
        itemViewModel.hideLoading = {
            DispatchQueue.main.async { self.activityIndicator.stopAnimating() }
        }
        itemViewModel.getData()
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemViewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductTableViewCell else {
            fatalError("Cell not exists in storyboard")
        }
        let cellVM = itemViewModel.getCellViewModel( at: indexPath )
        if let imageURL = URL(string: cellVM.image) {
            cell.productImageView.sd_setImage(with: imageURL)
        }
        cell.productNameLabel.text = cellVM.displayName
        if (cellVM.listPrice == cellVM.salePrice) {
            cell.regularPriceLabel.text = ""
        } else {
            let amount = String.toCurrency(amount: cellVM.listPrice)
            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: amount)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
            cell.regularPriceLabel.attributedText = attributeString
        }
        cell.salePriceLabel.text = String.toCurrency(amount: cellVM.salePrice)
        return cell
    }
}
