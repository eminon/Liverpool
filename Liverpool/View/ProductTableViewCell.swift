//
//  ProductTableViewCell.swift
//  Liverpool
//
//  Created by Eduardo Miñon on 16/02/24.
//

import UIKit
import SDWebImage

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var regularPriceLabel: UILabel!
    @IBOutlet weak var salePriceLabel: UILabel!
    
}
