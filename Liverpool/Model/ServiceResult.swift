//
//  ServiceResult.swift
//  Liverpool
//
//  Created by Eduardo Miñon on 16/02/24.
//

import Foundation

struct ServiceResult: Codable {
    let nullPageContent: Array<Content>
}

struct BannerContent: Codable {
    let type: String
}

struct CarouselContent: Codable {
    let type: String
    let carouselContent: CarouselItems
}

struct CarouselItems: Codable {
    let recommendedItems: Array<RecommendedItem>
}

struct RecommendedItem: Codable {
    let largeImage: String
    let displayName: String
    let listPrice: Float
    let salePrice: Float
}

enum ContentType: String, Codable {
    case banner
    case carousel
}

enum Content : Codable {
    case carousel(CarouselContent), banner (BannerContent)
    
    enum CodingKeys : String, CodingKey { case type }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try  container.decode(ContentType.self, forKey: .type)
        let singleContainer = try decoder.singleValueContainer()
        switch type {
            case .banner: self = .banner(try singleContainer.decode(BannerContent.self))
            case .carousel: self = .carousel(try singleContainer.decode(CarouselContent.self))
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
            case .banner(let banner): try container.encode(banner)
            case .carousel(let carousel): try container.encode(carousel)
        }
    }
}
