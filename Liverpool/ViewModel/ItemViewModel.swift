//
//  ItemViewModel.swift
//  Liverpool
//
//  Created by Eduardo Miñon on 16/02/24.
//

import Foundation

class ItemViewModel {
    var listItems: [RecommendedItem] = [RecommendedItem]()
    var reloadTableView: (()->())?
    var showError: (()->())?
    var showLoading: (()->())?
    var hideLoading: (()->())?
    
    private var cellViewModels: [ProductCellViewModel] = [ProductCellViewModel]() {
        didSet {
            self.reloadTableView?()
        }
    }
    
    func getData() {
        showLoading?()
        ApiClient.getDataFromServer { (success, data) in
            self.hideLoading?()
            if success {
                var result = [RecommendedItem]()
                _ = data!.nullPageContent.last { item in
                    switch item {
                    case .banner(_):
                        return false
                    case .carousel(let carousel):
                        result = carousel.carouselContent.recommendedItems
                        return true
                    }
                }
                self.createCell(response: result)
                self.reloadTableView?()
            } else {
                self.showError?()
            }
        }
    }
    
    var numberOfCells: Int {
        return cellViewModels.count
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> ProductCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func createCell(response: [RecommendedItem]){
        self.listItems = response
        var cells = [ProductCellViewModel]()
        
        for item in response {
            cells.append(ProductCellViewModel(image: item.largeImage, displayName: item.displayName, listPrice: item.listPrice, salePrice: item.salePrice))
        }
        cellViewModels = cells
    }
}


struct ProductCellViewModel {
    let image: String
    let displayName: String
    let listPrice: Float
    let salePrice: Float
}
