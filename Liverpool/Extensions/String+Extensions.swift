//
//  String+Extensions.swift
//  Liverpool
//
//  Created by Eduardo Miñon on 16/02/24.
//

import Foundation

extension String {
    static func toCurrency(amount: Float) -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.current
        return currencyFormatter.string(from: amount as NSNumber) ?? ""
    }
}
