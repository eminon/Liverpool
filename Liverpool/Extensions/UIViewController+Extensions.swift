//
//  UIViewController+Extensions.swift
//  Liverpool
//
//  Created by Eduardo Miñon on 16/02/24.
//

import UIKit

extension UIViewController {
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
