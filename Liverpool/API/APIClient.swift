//
//  APIClient.swift
//  Liverpool
//
//  Created by Eduardo Miñon on 16/02/24.
//

import Foundation

public struct ApiClient {
    static func getDataFromServer( complete: @escaping (_ success: Bool, _ data: ServiceResult? )->() ){
        guard let url = URL(string:"https://shoppapp.liverpool.com.mx/appclienteservices/services/v7/plp/sf?page-number=1&search-string=%7B%7Btermino-de-busqueda%7D%7D&sort-option=&force-plp=false&number-of-items-per-page=40&cleanProductName=false") else {
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let json = data else {
                return
            }
            do {
                let decoder = JSONDecoder()
                let result = try decoder.decode(ServiceResult.self, from: json)
                complete(true, result)
            } catch let error {
                debugPrint(error)
                print("Ha ocurrido un error: \(error.localizedDescription)")
                complete(false, nil)
            }
        }.resume()
    }
}
